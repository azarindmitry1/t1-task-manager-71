package ru.t1.azarin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.util.NumberUtil;

@Component
public final class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    public final static String NAME = "info";

    @NotNull
    public final static String ARGUMENT = "-i";

    @NotNull
    public final static String DESCRIPTION = "Display process and memory info.";

    @Override
    @EventListener(condition = "@systemInfoListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
