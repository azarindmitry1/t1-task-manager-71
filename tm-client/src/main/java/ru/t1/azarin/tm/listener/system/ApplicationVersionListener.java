package ru.t1.azarin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.azarin.tm.dto.response.system.ApplicationVersionResponse;
import ru.t1.azarin.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public final static String NAME = "version";

    @NotNull
    public final static String ARGUMENT = "-v";

    @NotNull
    public final static String DESCRIPTION = "Display application version.";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
