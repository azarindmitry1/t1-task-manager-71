package ru.t1.dazarin.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dazarin.tm.api.repository.UserDtoRepository;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.exception.entity.UserNotFoundException;
import ru.t1.dazarin.tm.exception.field.LoginEmptyException;
import ru.t1.dazarin.tm.exception.field.PasswordEmptyException;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.UserDto;
import ru.t1.dazarin.tm.service.dto.UserDtoService;

import javax.transaction.Transactional;

import static ru.t1.dazarin.tm.constant.UserTestData.*;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class UserDtoServiceTest {

    @NotNull
    @Autowired
    private UserDtoRepository userDtoRepository;

    @NotNull
    @Autowired
    private UserDtoService userDtoService;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @After
    public void tearDown() {
        userDtoRepository.deleteAll();
    }

    @Test
    public void createUser() {
        Assert.assertThrows(LoginEmptyException.class,
                () -> userDtoService.createUser("", TEST_USER_PASSWORD, USER_ROLE_TYPE));
        Assert.assertThrows(PasswordEmptyException.class,
                () -> userDtoService.createUser(TEST_USER_LOGIN, null, USER_ROLE_TYPE));
        Assert.assertThrows(UserNotFoundException.class, () -> userDtoService.findByLogin(TEST_USER_LOGIN));
        userDtoService.createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, USER_ROLE_TYPE);
        @NotNull final UserDto userDto = userDtoService.findByLogin(TEST_USER_LOGIN);
        Assert.assertEquals(TEST_USER_LOGIN, userDto.getLogin());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userDtoService.findByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userDtoService.findByLogin(TEST_USER_LOGIN));
        userDtoService.createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, USER_ROLE_TYPE);
        @NotNull final UserDto userDto = userDtoService.findByLogin(TEST_USER_LOGIN);
        Assert.assertEquals(TEST_USER_LOGIN, userDto.getLogin());
    }

}
