package ru.t1.dazarin.tm.integration.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.dazarin.tm.marker.IntegrationCategory;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.model.dto.Result;

import javax.transaction.Transactional;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Transactional
@Category(IntegrationCategory.class)
public class ProjectClientTest {

    private static final String BASEURL = "http://localhost:8080/api/project";

    @NotNull
    private static HttpHeaders HEADERS = new HttpHeaders();

    @NotNull
    private static String SESSION_ID;

    @NotNull
    private ProjectDto projectOne;

    @BeforeClass
    @SneakyThrows
    public static void setUpBefore() {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?login=admin&password=admin";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(SESSION_ID);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void tearDownAfter() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    private static ResponseEntity<ProjectDto> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, ProjectDto.class);
    }

    @Before
    public void setUp() {
        @NotNull final String createUrl = BASEURL + "/create";
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(
                createUrl,
                HttpMethod.POST,
                new HttpEntity<>(HEADERS)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        projectOne = response.getBody();
        Assert.assertNotNull(projectOne);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String deleteAllUrl = BASEURL + "/deleteAll";
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
    }

    @Test
    public void create() {
        @NotNull final String url = BASEURL + "/create";
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(
                url,
                HttpMethod.POST,
                new HttpEntity<>(HEADERS)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void findById() {
        @NotNull final String projectId = projectOne.getId();
        @NotNull final String findByIdUrl = BASEURL + "/findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String findAllUrl = BASEURL + "/findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDto[]> response =
                (template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(HEADERS), ProjectDto[].class));
        Assert.assertNotNull(response.getBody());
        ProjectDto[] projects = response.getBody();
        Assert.assertTrue(Arrays.stream(projects).anyMatch(p -> projectOne.getId().equals(p.getId())));
    }

    @Test
    public void update() {
        @NotNull final String updateUrl = BASEURL + "/update";
        @NotNull final String projectId = projectOne.getId();
        projectOne.setName("TEST NAME");
        sendRequest(updateUrl, HttpMethod.PUT, new HttpEntity<ProjectDto>(projectOne, HEADERS));
        @NotNull final String findUrl = BASEURL + "/findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDto> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals("TEST NAME", projectDto.getName());
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = projectOne.getId();
        @NotNull final String deleteByIdUrl = BASEURL + "/deleteById/" + projectId;
        @NotNull final String findByIdUrl = BASEURL + "/findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(deleteByIdUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
        response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteAll() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(projectOne);
        @NotNull final String projectId = projectOne.getId();
        @NotNull final String deleteAllUrl = BASEURL + "/deleteAll";
        @NotNull final String findByIdUrl = BASEURL + "/findById/" + projectId;
        @NotNull ResponseEntity<ProjectDto> response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDto projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(deleteAllUrl, HttpMethod.DELETE, new HttpEntity<>(projects, HEADERS));
        response = sendRequest(findByIdUrl, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertNull(response.getBody());
    }

}
