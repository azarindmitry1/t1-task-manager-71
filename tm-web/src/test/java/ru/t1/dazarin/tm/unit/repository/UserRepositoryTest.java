package ru.t1.dazarin.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dazarin.tm.api.repository.UserDtoRepository;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.UserDto;

import javax.transaction.Transactional;

import static ru.t1.dazarin.tm.constant.UserTestData.TEST_USER_LOGIN;
import static ru.t1.dazarin.tm.constant.UserTestData.TEST_USER_PASSWORD;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class UserRepositoryTest {

    @NotNull
    @Autowired
    private UserDtoRepository userDtoRepository;

    @NotNull
    private UserDto testUser;

    @Before
    public void setUp() {
        testUser = new UserDto();
        testUser.setLogin(TEST_USER_LOGIN);
        testUser.setPasswordHash(TEST_USER_PASSWORD);
        userDtoRepository.save(testUser);
    }

    @After
    public void tearDown() {
        userDtoRepository.delete(testUser);
    }

    @Test
    public void findByLogin() {
        @Nullable final UserDto userDto = userDtoRepository.findByLogin(TEST_USER_LOGIN);
        Assert.assertNotNull(userDto);
        Assert.assertEquals(TEST_USER_LOGIN, userDto.getLogin());
    }

}
