package ru.t1.dazarin.tm.exception.user;

public final class UserIsLockedException extends AbstractUserException {

    public UserIsLockedException() {
        super("Error! User is locked...");
    }

}
