package ru.t1.dazarin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

@Controller
public class TaskController {

    @Autowired
    private ProjectDtoService projectDtoService;

    @Autowired
    private TaskDtoService taskDtoService;

    @PostMapping("/task/create")
    public String create() {
        taskDtoService.create(UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskDtoService.deleteById(UserUtil.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/update/{id}")
    public ModelAndView update(@PathVariable("id") String id) {
        final TaskDto taskDto = taskDtoService.findById(UserUtil.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-update");
        modelAndView.addObject("task", taskDto);
        modelAndView.addObject("projects", projectDtoService.findAll(UserUtil.getUserId()));
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping("/task/update/{id}")
    public String update(@ModelAttribute("task") TaskDto task) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) task.setProjectId(null);
        taskDtoService.save(UserUtil.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/tasks")
    public ModelAndView index() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("projectDtoService", projectDtoService);
        modelAndView.addObject("tasks", taskDtoService.findAll(UserUtil.getUserId()));
        return modelAndView;
    }

}
