package ru.t1.dazarin.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import java.util.List;

public interface ProjectClient {

    static ProjectClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @GetMapping(value = "/findAll")
    List<ProjectDto> findAll();

    @GetMapping(value = "/findById/{id}")
    ProjectDto findById(@NotNull @PathVariable("id") String id);

    @PostMapping(value = "/create")
    ProjectDto create();

    @DeleteMapping(value = "/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @DeleteMapping(value = "/deleteAll")
    void deleteAll();

    @PutMapping(value = "/update")
    ProjectDto update(@NotNull @RequestBody ProjectDto projectDto);

}
