package ru.t1.dazarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.dazarin.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private ProjectDtoService projectDtoService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public ProjectDto create() {
        return projectDtoService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping(value = "/findAll")
    public List<ProjectDto> findAll() {
        return projectDtoService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping(value = "/findById/{id}")
    public ProjectDto findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        return projectDtoService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping(value = "/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        projectDtoService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping(value = "/deleteAll")
    public void deleteAll() {
        projectDtoService.deleteAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PutMapping(value = "/update")
    public ProjectDto update(
            @WebParam(name = "projectDto", partName = "projectDto")
            @NotNull @RequestBody final ProjectDto projectDto
    ) {
        return projectDtoService.save(UserUtil.getUserId(), projectDto);
    }

}
