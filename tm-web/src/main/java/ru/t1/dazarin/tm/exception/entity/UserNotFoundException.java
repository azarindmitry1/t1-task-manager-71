package ru.t1.dazarin.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
