package ru.t1.dazarin.tm.exception.user;

public final class ExistLoginException extends AbstractUserException {

    public ExistLoginException() {
        super("Error! Login is exist...");
    }

}
