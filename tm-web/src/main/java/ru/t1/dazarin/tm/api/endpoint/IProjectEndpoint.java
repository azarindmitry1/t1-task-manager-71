package ru.t1.dazarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/project")
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping(value = "/findAll")
    List<ProjectDto> findAll();

    @WebMethod
    @GetMapping(value = "/findById/{id}")
    ProjectDto findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping(value = "/create")
    ProjectDto create();

    @WebMethod
    @DeleteMapping(value = "/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id
    );

    @WebMethod
    @DeleteMapping(value = "/deleteAll")
    void deleteAll();

    @WebMethod
    @PutMapping(value = "/update")
    ProjectDto update(
            @WebParam(name = "projectDto", partName = "projectDto")
            @NotNull @RequestBody ProjectDto projectDto
    );

}
