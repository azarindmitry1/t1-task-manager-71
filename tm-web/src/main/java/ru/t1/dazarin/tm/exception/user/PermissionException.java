package ru.t1.dazarin.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission denied...");
    }

}
