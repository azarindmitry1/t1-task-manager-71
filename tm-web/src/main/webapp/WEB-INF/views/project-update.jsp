<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>EDIT PROJECT</h1>

<form:form action="/project/update/${project.id}" method="POST" modelAttribute="project">

    <form:input type="hidden" path="id"/>
    <form:input type="hidden" path="userId"/>

    <p>
    <div>
        <span>NAME:</span>
    </div>
    <div>
        <form:input type="text" path="name"/>
    </div>
    </p>

    <p>
    <div>
        <span>DESCRIPTION:</span>
    </div>
    <div>
        <form:input type="text" path="description"/>
    </div>
    </p>

    <p>
    <div>
        <span>STATUS:</span>
    </div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    </p>

    <p>
    <div>
        <span>DATE BEGIN:</span>
    </div>
    <div>
        <form:input type="date" path="dateStart"/>
    </div>
    </p>

    <p>
    <div>
        <span>DATE FINISH:</span>
    </div>
    <div>
        <form:input type="date" path="dateFinish"/>
    </div>
    </p>

    <button type="submit">SAVE PROJECT</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>
