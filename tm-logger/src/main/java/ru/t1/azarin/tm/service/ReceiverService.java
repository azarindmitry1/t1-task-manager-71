package ru.t1.azarin.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.IReceiverService;
import ru.t1.azarin.tm.listener.EntityListener;

import javax.jms.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ReceiverService implements IReceiverService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @Override
    @SneakyThrows
    public void receive(@NotNull @Autowired final EntityListener entityListener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
